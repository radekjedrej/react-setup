class VisibilityToggle extends React.Component {


  constructor(props) {
    super(props);

    this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
    this.state = {
      visibility: false,
      heading: 'Visibility Toggle',
      text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit, et.'
    }
  }

  handleToggleVisibility() {
    this.setState((prevState) => {
      return {
        visibility: !prevState.visibility
      }
    })
  }
  
  render() {
    return (
      <div>
        <h1>{this.state.heading}</h1>
        <button onClick={this.handleToggleVisibility}>{this.state.visibility ? 'Hide details' : 'Show details'}</button>
        { this.state.visibility && (
          <p>{this.state.text}</p>
        )}
      </div>
    )
  }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById('app'));

