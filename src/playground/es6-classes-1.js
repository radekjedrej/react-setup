
class Person {
  constructor(name = 'Anonymous', age = 0) {
    this.name = name;
    this.age = age;
  }
  getGreeting() {
    return `Hi. I am ${this.name}!`;
  }

  getDescription() {
    // return 'Hi I am ' + this.name + '!';
    return `Hi. I am ${this.age} years old`
  }
}

class Student extends Person {
  constructor(name, age, major) {
    super(name, age);
    this.major = major;
  }
  hasMajor() {
    return !!this.major;
  }
  getDescription() {
    let description = super.getDescription();

    if (this.hasMajor()) {
      description += ` There major is ${this.major}`;
    }

    return description;
  }
}

class Traveler extends Person {
  constructor(name, age, homeLocation) {
    super(name, age);
    this.homeLocation = homeLocation;
  }

  hasHomeLocation() {
    return !!this.homeLocation;
  }

  getGreeting() {
    let getGreeting = super.getGreeting();
    
    if(this.hasHomeLocation()) {
      getGreeting += ` I'm visiting from ${this.homeLocation}`;
    }

    return getGreeting;
  }
}

const mike = new Traveler('Mike', 26, 'Sopot');
console.log(mike.getGreeting());  

const seba = new Traveler('seba', 26);
console.log(seba.getGreeting());  


// const me = new Student('Radek Jedrej', 31, 'Web Developer');
// const you = new Student();

// console.log(me.getDescription());
// console.log(you.getDescription());
